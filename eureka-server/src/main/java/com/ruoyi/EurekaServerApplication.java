package com.ruoyi;

import com.netflix.appinfo.InstanceInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.cloud.netflix.eureka.server.event.*;
import org.springframework.context.event.EventListener;

@Slf4j
@SpringBootApplication
@EnableEurekaServer
public class EurekaServerApplication {

    // 服务器启动时触发
    @EventListener
    public void listen(EurekaServerStartedEvent event) {
        log.info("服务器启动时触发,{}", System.currentTimeMillis());
    }

    // 服务器可用时触发
    @EventListener
    public void listen(EurekaRegistryAvailableEvent event) {
        log.info("服务器可用时触发,{}", System.currentTimeMillis());
    }

    // 服务注册时触发
    @EventListener
    public void listen(EurekaInstanceRegisteredEvent event) {
        InstanceInfo instanceInfo = event.getInstanceInfo();
        log.info(
                "服务{}进行注册",
                instanceInfo.getAppName()
                        + instanceInfo.getHostName()
                        + "  "
                        + instanceInfo.getIPAddr()
                        + "  "
                        + instanceInfo.getPort());
    }

    // 服务续约时触发
    @EventListener
    public void listen(EurekaInstanceRenewedEvent event) {
        //    log.info("服务{}进行续约", event.getServerId() + "  " + event.getAppName());
    }

    // 服务下线时触发
    @EventListener
    public void listen(EurekaInstanceCanceledEvent event) {
        log.info("服务{}已下线", event.getAppName());
        log.info("server地址信息{}", event.getServerId());
    }

    public static void main(String[] args) {
        SpringApplication.run(EurekaServerApplication.class, args);
    }
}
